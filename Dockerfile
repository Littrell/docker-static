FROM node:8

RUN set -x \
    && sed -i 's/# \(.*multiverse\)/\1/g' /etc/apt/sources.list \
    && apt-get update \
    && apt-get -y upgrade

RUN set -x \
    && npm -g install http-server

WORKDIR /workspace
