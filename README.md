# Description
docker-static is a Docker container that serves the current directory by volume mounting and utilizing [http-server] (https://www.npmjs.com/package/http-server). If the current directory contains an index.html file, that file will be served. Otherwise this displays a directory listing.   

# Setup

After cloning, apply the following alias
```
alias static="/bin/bash <path to repository directory>/build.sh"
```

[build.sh] (https://gitlab.com/Littrell/docker-static/blob/master/build.sh) is a helper script used to setup volume mounting and ports.   

If you would rather use Docker directly, use the following:
```shell
docker build -t http-server:latest .

docker run -p <port>:<port> -v $PWD:/workspace --name="http-server-<port>" -i -t http-server:latest http-server . -p <port>
```